'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('pickup_points', [
      { cityId: 77, address: 'Театральный пр-д, 5с1, Москва, 109012' },
      { cityId: 77, address: 'Садовая-Кудринская ул., 7, Москва, 123001' },
      { cityId: 77, address: 'Большая Серпуховская ул., 31к11, Москва, 117162' },
      { cityId: 77, address: 'ул. Большая Полянка, 30, Москва, 119180' }
    ], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
