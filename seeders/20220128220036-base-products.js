'use strict';

// truncate & drop sequence

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.sequelize.truncate('products')
    await queryInterface.sequelize.truncate('ingredients')
    await queryInterface.sequelize.query('ALTER SEQUENCE "products_id_seq" RESTART WITH 1;');
    await queryInterface.sequelize.query('ALTER SEQUENCE "ingredients_id_seq" RESTART WITH 1;');

    await queryInterface.bulkInsert('products', [{
      name: 'Колбаски Барбекю',
      description: 'Острая Чоризо, соус барбекю, томаты, красный лук, моцарелла, томатный соус',
      price: 395,
      currency: 'р.',
      energyValue: '700',
      isForQuick: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
      {
        name: 'Индейка в мандаринах',
        description: 'Пастрами из индейки, соус альфредо, мандарины, цитрусовый соус, моцарелла, смесь сыров чеддер и пармезан',
        price: 495,
        currency: 'р.',
        energyValue: '800',
        isForQuick: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Сырная',
        description: 'Моцарелла, сыры чеддер и пармезан, соус альфредо',
        price: 245,
        currency: 'р.',
        energyValue: '500',
        isForQuick: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }], {})

    await queryInterface.bulkInsert('ingredients', [
      { name: 'Острая Чоризо', energyValue: '100' },
      { name: 'Соус барбекю', energyValue: '100' },
      { name: 'Томаты', energyValue: '100' },
      { name: 'Красный лук', energyValue: '100' },
      { name: 'Моцарелла', energyValue: '100' },
      { name: 'Томатный соус', energyValue: '100' },
      { name: 'Пастрами из индейки', energyValue: '100' },
      { name: 'Соус альфредо', energyValue: '100' },
      { name: 'Мандарины', energyValue: '100' },
      { name: 'Цитрусовый соус', energyValue: '100' },
      { name: 'Смесь сыров чеддер и пармезан', energyValue: '100' },
      { name: 'Соус альфредо', energyValue: '100' }
    ], {})

    await queryInterface.bulkInsert('products_ingredients', [
      { productId: 1, ingredientId: 1 },
      { productId: 1, ingredientId: 2 },
      { productId: 1, ingredientId: 3 },
      { productId: 1, ingredientId: 4 },
      { productId: 1, ingredientId: 5 },
      { productId: 2, ingredientId: 5 },
      { productId: 3, ingredientId: 5 },
      { productId: 1, ingredientId: 6 },
      { productId: 2, ingredientId: 7 },
      { productId: 2, ingredientId: 8 },
      { productId: 2, ingredientId: 9 },
      { productId: 2, ingredientId: 10 },
      { productId: 2, ingredientId: 11 },
      { productId: 3, ingredientId: 11 },
      { productId: 3, ingredientId: 12 }
    ], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
