'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('cities', [{
      id: 77,
      name: "Москва",
      type: "г",
      name_with_type: "г Москва",
      federal_district: "Центральный",
      kladr_id: "7700000000000",
      fias_id: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
      okato: "45000000000",
      oktmo: "45000000",
      tax_office: "7700",
      postal_code: "101000",
      iso_code: "RU-MOW",
      timezone: "UTC+3",
      geoname_code: "RU.48",
      geoname_id: "524894",
      geoname_name: "Moscow"
    }], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
