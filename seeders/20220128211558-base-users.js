'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('users', [{
      name: 'TestUser',
      phone: '+79605897860',
      password: 'a634f92c80b47d87cf9cf2aa8a6d90c7eb94e3e5',
      email: 'Alexander.Sokolov@waveaccess.ru',
      dodoCode: 'testCode',
      birthday: '1992-08-30 00:00:00.548408+00',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
