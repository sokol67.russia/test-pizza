Описание API:

- Endpoint, который будет позволять логиниться (по номеру телефона):
POST: { "phone": "+79605897860", "password": "12345"}
127.0.0.1:3000/api/auth/login

- Endpoint с выдачей списка городов. Структура: { id, name }:
GET
127.0.0.1:3000/api/cities

- Endpoint выдающий список мест самовывоза для выбранного города (таблицей в базе):
GET
127.0.0.1:3000/api/pickup-points/city?cityId=77

- Endpoint для проверки промокода:
POST: { "code": "TEST_PROMO" }
127.0.0.1:3000/api/promo-codes/

- Endpoint выдающий текущий личные данные:
GET
127.0.0.1:3000/api/users/me

- Endpoint сохраняющий отправленные личные данные:
POST: { "name": "Test1" }
127.0.0.1:3000/api/users/me

- Endpoint для получения всех товаров:
GET
127.0.0.1:3000/api/products

- Endpoint для получения более детальной карточки товара плюс так же список добавок для него:
GET
127.0.0.1:3000/api/products/detail?id=3
127.0.0.1:3000/api/products/3?with=ingredients

- Endpoint для списка товаров быстрого заказа. (есть товары с пометкой "для быстрого заказа"):
GET
127.0.0.1:3000/api/products/products/quick

- Endpoint для принятия заказа. В ответ высылается номер заказа:
POST
127.0.0.1:3000/api/orders/save
{
  "userId": 1,
  "cityId": 77,
  "address": "test address",
  "sum": 12345,
  "currency": "р.",
  "products": [
    { "id": 1 },
    { "id": 3 }
  ],
  "ingredients": [{ "id": 13 }]
}

- Endpoint со списком всех заказов:
GET
127.0.0.1:3000/api/orders

- Endpoint сохранить новый или существующий заказ (обновление статуса):
patch
127.0.0.1:3000/api/orders/:id
{
  "status": "inDelivery"
}

- Endpoint выдающий детальную инфу по конкретному заказу:
GET
127.0.0.1:3000/api/orders/:id?with[]=products&with[]=ingredients

- Endpoint удалить заказ:
delete
127.0.0.1:3000/api/orders/:id

