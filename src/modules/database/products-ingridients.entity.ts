import {AllowNull, BelongsToMany, Column, ForeignKey, Model, Table} from "sequelize-typescript"
import Product from "@/modules/products/product.entity";
import Ingredient from "@/modules/Ingredients/ingridient.entity";
import {Association} from "sequelize";

@Table({
  modelName: 'products_ingredients',
  timestamps: false
})
export class ProductsIngredients extends Model<ProductsIngredients> {
  @ForeignKey(() => Product)
  @AllowNull(false)
  @Column
  productId: number

  @ForeignKey(() => Ingredient)
  @AllowNull(false)
  @Column
  ingredientId: number
}

export default ProductsIngredients
