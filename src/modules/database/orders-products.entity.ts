import {AllowNull, Column, ForeignKey, Model, Table} from "sequelize-typescript"
import Order from "@/modules/orders/order.entity";
import Product from "@/modules/products/product.entity";

@Table({
  modelName: 'orders_products',
  timestamps: false
})
export class OrdersProducts extends Model<OrdersProducts> {
  @ForeignKey(() => Product)
  @AllowNull(false)
  @Column
  productId: number

  @ForeignKey(() => Order)
  @AllowNull(false)
  @Column
  orderId: number
}

export default OrdersProducts;
