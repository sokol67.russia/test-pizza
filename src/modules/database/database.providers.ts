import { Sequelize, SequelizeOptions } from 'sequelize-typescript'
import * as path from 'path'
import { FactoryProvider } from '@nestjs/common/interfaces'
import {EnvironmentService} from "@/modules/environment/environment.service";

export const sequelizeConnectionProvider: FactoryProvider = {
  provide: 'SequelizeConnection',
  async useFactory(envService: EnvironmentService) {
    const ssl: boolean = ['1', 'true', 'Y'].includes(envService.get('POSTGRES_SSL', 'false'))
    const options: SequelizeOptions = {
      dialect: 'postgres',
      host: envService.get('POSTGRES_HOST', 'localhost'),
      port: Number(envService.get('POSTGRES_PORT', 5432)),
      username: envService.get('POSTGRES_USER', 'admin'),
      password: envService.get('POSTGRES_PASSWORD', 'admin'),
      database: envService.get('POSTGRES_DB', 'db'),
      logging: false,
      models: [path.resolve(__dirname, '../../modules') + '/**/*.entity{.ts,.js}']
    }

    if (ssl) {
      options.dialectOptions = { ssl: true }
    }

    const sequelize = new Sequelize(options)
    await sequelize.sync()
    return sequelize
  },
  inject: [EnvironmentService]
}

export const databaseProviders = [sequelizeConnectionProvider]
