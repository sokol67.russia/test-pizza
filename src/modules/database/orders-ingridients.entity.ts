import {AllowNull, Column, ForeignKey, Model, Table} from "sequelize-typescript"
import Ingredient from "@/modules/Ingredients/ingridient.entity";
import Order from "@/modules/orders/order.entity";

@Table({
  modelName: 'orders_ingredients',
  timestamps: false
})
export class OrdersIngredients extends Model<OrdersIngredients> {
  @ForeignKey(() => Order)
  @AllowNull(false)
  @Column
  orderId: number

  @ForeignKey(() => Ingredient)
  @AllowNull(false)
  @Column
  ingredientId: number

}

export default OrdersIngredients
