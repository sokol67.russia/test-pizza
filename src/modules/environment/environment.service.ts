import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class EnvironmentService extends ConfigService {
  getPort(): number {
    return Number(this.get('PORT', 3000))
  }
}
