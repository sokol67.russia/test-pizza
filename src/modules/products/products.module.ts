import {forwardRef, Module} from '@nestjs/common'
import { ProductsService } from "@/modules/products/products.service";
import {ProductsController} from "@/modules/products/products.controller";
import {productsProviders} from "@/modules/products/products.providers";
import {IngredientsModule} from "@/modules/Ingredients/ingridients.module";

@Module({
  imports: [forwardRef(() => IngredientsModule)],
  controllers: [ProductsController],
  providers: [ProductsService, ...productsProviders],
  exports: [ProductsService, ...productsProviders]
})
export class ProductsModule {}
