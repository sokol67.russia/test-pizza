import {
  AllowNull,
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  CreatedAt,
  Default,
  DeletedAt, HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt
} from 'sequelize-typescript'
import Order from '@/modules/orders/order.entity'
import OrderProduct from "@/modules/database/orders-products.entity"
import {Association, BelongsToManyGetAssociationsMixin, HasManyGetAssociationsMixin} from "sequelize";
import Ingredient from "@/modules/Ingredients/ingridient.entity";
import ProductsIngredients from "@/modules/database/products-ingridients.entity";

@Table({
  modelName: 'products'
})
export class Product extends Model<Product> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  name!: string

  @Column
  description!: string

  @Default(0)
  @AllowNull(false)
  @Column
  price: number

  @Column
  public currency: string

  @Column
  public energyValue: string

  @Column
  public isForQuick: boolean

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  @DeletedAt
  deletedAt: Date

  @BelongsToMany(() => Order, () => OrderProduct)
  orders: Order[]

  @BelongsToMany(() => Ingredient, () => ProductsIngredients)
  ingredients: Ingredient[]

  getIngredients!: BelongsToManyGetAssociationsMixin<Ingredient>

  static associations: {
    ingredients: Association<Product, Ingredient>
  }
}

export default Product
