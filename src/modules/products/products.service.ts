import {forwardRef, Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from "@/lib/sequelize.service";
import Product from "@/modules/products/product.entity";
import {IngredientsService} from "@/modules/Ingredients/ingridients.service";
import {productsRepositoryProvider} from "@/modules/products/products.providers";

@Injectable()
export class ProductsService extends SequelizeService<Product> {
  constructor(
    @Inject(productsRepositoryProvider.provide)
    readonly repository: typeof Product,
    @Inject(forwardRef(() => IngredientsService))
    protected readonly ingredientsService: IngredientsService,
  ) {
    super(repository)
  }
}
