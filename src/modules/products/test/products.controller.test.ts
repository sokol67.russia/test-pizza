import {Test} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {ProductsController} from "../products.controller";
import {ProductsService} from "../products.service";
import Product from "../product.entity";
import Ingredient from "../../Ingredients/ingridient.entity";

const moduleMocker = new ModuleMocker(global)

describe('ProductsController', () => {
  let productsController: ProductsController
  let productsService: ProductsService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ProductsController],
      // providers: [ProductsService],
    }).useMocker((token) => {
        if (token === ProductsService) {
          return {
            get: jest.fn().mockResolvedValue(Promise.resolve({} as Product)),
            find: jest.fn().mockResolvedValue(Promise.resolve([]))
          }
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        }
      }).compile()

    productsService = moduleRef.get<ProductsService>(ProductsService)
    productsController = moduleRef.get<ProductsController>(ProductsController)
  })

  describe('getDetail', () => {
    it('should call the get service method', async () => {
      const result = await productsController.getDetail(1)
      expect(productsService.get).toHaveBeenCalled()
      expect(productsService.get).toHaveBeenCalledWith(1, { include: [Ingredient] })
      expect(result).toEqual(expect.objectContaining({}))
    })
  })

  describe('getQuick', () => {
    it('should call the find service method', async () => {
      const result = await productsController.getQuick()
      expect(productsService.find).toHaveBeenCalled()
      expect(productsService.find).toHaveBeenCalledWith({ where: { isForQuick: true } })
      expect(result).toEqual(expect.objectContaining([]))
    })
  })
})
