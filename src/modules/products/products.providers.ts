import Product from '@/modules/products/product.entity'
import { Provider, ValueProvider } from '@nestjs/common/interfaces'

export const productsRepositoryProvider: ValueProvider = {
  provide: 'ProductsRepository',
  useValue: Product
}

export const productsProviders: Provider[] = [productsRepositoryProvider]
