import {Body, Controller, Get, Patch, Query, Req} from '@nestjs/common'
import {SequelizeCrudController} from '@/lib/sequelize-crud.controller'
import {ProductsService} from '@/modules/products/products.service'
import Product from "@/modules/products/product.entity"
import {Request} from "express";
import Ingredient from "@/modules/Ingredients/ingridient.entity";

@Controller('products')
export class ProductsController extends SequelizeCrudController<ProductsService, Product> {
  constructor(readonly service: ProductsService) {
    super(service)
  }

  @Get('/detail')
  async getDetail (@Query('id') id: number) {
    // @ts-ignore
    return this.service.get(id, { include: [Ingredient] })
  }

  @Get('/quick')
  async getQuick () {
    return this.service.find({ where: { isForQuick: true } })
  }
}
