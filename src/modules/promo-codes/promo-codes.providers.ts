import { Provider, ValueProvider } from '@nestjs/common/interfaces'
import PromoCode from '@/modules/promo-codes/promo-code.entity'

export const promoCodesRepositoryProvider: ValueProvider = {
  provide: 'PromoCodesRepository',
  useValue: PromoCode
}

export const promoCodesProviders: Provider[] = [promoCodesRepositoryProvider]
