import {Test} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {PromoCodesController} from "@/modules/promo-codes/promo-codes.controller";
import {PromoCodesService} from "@/modules/promo-codes/promo-codes.service";

const moduleMocker = new ModuleMocker(global)

describe('PromoCodesController', () => {
  let promoCodesController: PromoCodesController
  let promoCodesService: PromoCodesService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [PromoCodesController],
      // providers: [PromoCodesService],
    }).useMocker((token) => {
        if (token === PromoCodesService) {
          return {
            check: jest.fn().mockResolvedValue(Promise.resolve(true)),
          }
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        }
      }).compile()

    promoCodesService = moduleRef.get<PromoCodesService>(PromoCodesService)
    promoCodesController = moduleRef.get<PromoCodesController>(PromoCodesController)
  })

  describe('check', () => {
    it('should call the check service method', async () => {
      const result = await promoCodesController.check('code')
      expect(promoCodesService.check).toHaveBeenCalled()
      expect(promoCodesService.check).toHaveBeenCalledWith('code')
      expect(result).toEqual(true)
    })
  })
})
