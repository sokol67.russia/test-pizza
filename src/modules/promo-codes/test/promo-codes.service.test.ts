import {Test, TestingModule} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {UsersService} from "@/modules/users/users.service"
import {AuthService} from "@/modules/auth/auth.service";
import {PromoCodesService} from "../promo-codes.service";
import PromoCode from "../promo-code.entity";
import {PromoCodesController} from "../promo-codes.controller";

const moduleMocker = new ModuleMocker(global)
const testCode = {
  id: 1,
  code: 'testCode',
  isActive: true,
} as PromoCode

describe('PromoCodesService', () => {
  let promoCodesService: PromoCodesService

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [PromoCodesController],
      providers: [PromoCodesService],
    }).useMocker((token) => {
      switch (token) {
        case 'PromoCodesRepository': return {}
        case AuthService: return {}
        case UsersService: return {}
        case 'function':
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        default:
          break
      }
    }).compile()

    promoCodesService = moduleRef.get<PromoCodesService>(PromoCodesService)
  })

  describe('check', () => {
    it('Should check either exist specified promo-code or not', async () => {
      // jest.fn().mockImplementation((phone) => phone === '+79605897860' ? testUser : undefined)
      jest.spyOn(promoCodesService, 'find').mockImplementation(({ where }) =>
      // @ts-ignore
        Promise.resolve((where.code === testCode && where.isActive) ? [testCode] : []))

      // TODO::FIX
      // @ts-ignore
      const result = await promoCodesService.check(testCode)
      expect(promoCodesService.find).toHaveBeenCalled()
      expect(promoCodesService.find).toHaveBeenCalledWith({ where: { code: testCode, isActive: true } })
      expect(result).toEqual(true)

      const falseResult = await promoCodesService.check('false')
      expect(promoCodesService.find).toHaveBeenCalled()
      expect(promoCodesService.find).toHaveBeenCalledWith({ where: { code: 'false', isActive: true } })
      expect(falseResult).toEqual(false)
    })
  })
})

