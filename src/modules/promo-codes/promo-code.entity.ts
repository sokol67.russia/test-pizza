import {AllowNull, AutoIncrement, Column, Default, Model, PrimaryKey, Table} from 'sequelize-typescript'

@Table({
  modelName: 'promo_codes'
})
export class PromoCode extends Model<PromoCode> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  code!: string

  @Default(false)
  @Column
  isActive: boolean
}

export default PromoCode
