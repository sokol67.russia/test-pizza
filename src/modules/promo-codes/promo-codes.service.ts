import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import PromoCode from '@/modules/promo-codes/promo-code.entity'
import {promoCodesRepositoryProvider} from "@/modules/promo-codes/promo-codes.providers";

@Injectable()
export class PromoCodesService extends SequelizeService<PromoCode> {
  constructor(
    @Inject(promoCodesRepositoryProvider.provide)
    readonly repository: typeof PromoCode,
  ) {
    super(repository)
  }

  async check (code) {
    const found = await this.find({ where: { code, isActive: true } })
    return !!found.length
  }
}
