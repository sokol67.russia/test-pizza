import {Body, Controller, Post} from '@nestjs/common'
import {SequelizeCrudController} from '@/lib/sequelize-crud.controller'
import {PromoCodesService} from '@/modules/promo-codes/promo-codes.service'
import PromoCode from "@/modules/promo-codes/promo-code.entity"

@Controller('promo-codes')
export class PromoCodesController extends SequelizeCrudController<PromoCodesService, PromoCode> {
  constructor(readonly service: PromoCodesService) {
    super(service)
  }

  @Post('/')
  async check (@Body('code') code: string) {
    return this.service.check(code)
  }
}
