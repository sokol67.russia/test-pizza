import { Module } from '@nestjs/common'
import { PromoCodesService } from '@/modules/promo-codes/promo-codes.service'
import {PromoCodesController} from '@/modules/promo-codes/promo-codes.controller'
import {promoCodesProviders} from '@/modules/promo-codes/promo-codes.providers'

@Module({
  imports: [],
  controllers: [PromoCodesController],
  providers: [PromoCodesService, ...promoCodesProviders],
  exports: [PromoCodesService, ...promoCodesProviders]
})
export class PromoCodesModule {}
