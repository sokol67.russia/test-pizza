import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import Role from "@/modules/variations/variation.entity";
import {variationsRepositoryProvider} from "@/modules/variations/variations.providers";

@Injectable()
export class VariationsService extends SequelizeService<Role> {
  constructor(
    @Inject(variationsRepositoryProvider.provide)
    readonly repository: typeof Role,
  ) {
    super(repository)
  }
}
