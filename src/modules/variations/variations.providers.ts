import { Provider, ValueProvider } from '@nestjs/common/interfaces'
import Role from "@/modules/variations/variation.entity";

export const variationsRepositoryProvider: ValueProvider = {
  provide: 'VariationsRepository',
  useValue: Role
}

export const variationsProviders: Provider[] = [variationsRepositoryProvider]
