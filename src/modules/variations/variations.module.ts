import {Module} from '@nestjs/common'
import {variationsProviders} from "@/modules/variations/variations.providers";
import {VariationsService} from "@/modules/variations/variations.service"

@Module({
  imports: [],
  // controllers: [VariationsController],
  providers: [VariationsService, ...variationsProviders],
  exports: [VariationsService, ...variationsProviders]
})
export class VariationsModule {}
