import {AllowNull, AutoIncrement, BelongsTo, Column, ForeignKey, Model, PrimaryKey, Table} from 'sequelize-typescript'
import {Association} from "sequelize";
import Product from "@/modules/variations/variation.entity";

@Table({
  modelName: 'variations'
})
export class Variation extends Model<Variation> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  size!: string

  @AllowNull(false)
  @Column
  weight!: string

  @ForeignKey(() => Product)
  @AllowNull(false)
  @Column
  productId!: number

  @BelongsTo(() => Product)
  product: Product

  static associations: {
    product: Association<Variation, Product>
  }
}

export default Variation
