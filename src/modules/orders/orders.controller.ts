import {Body, Controller, Param, Patch, Post} from '@nestjs/common'
import {SequelizeCrudController} from '@/lib/sequelize-crud.controller'
import {OrdersService} from '@/modules/orders/orders.service'
import Order from "@/modules/orders/order.entity"
import {CreateOptions} from "sequelize";

@Controller('orders')
export class OrdersController extends SequelizeCrudController<OrdersService, Order> {
  constructor(readonly service: OrdersService) {
    super(service)
  }

  @Post('/')
  // @ts-ignore
  async create(@Body() data: object, options?: CreateOptions<any>) {
    // @ts-ignore
    return this.service.createWithRelations(data, options)
  }

  // @Get('/detail')
  // async getDetail (@Query('id') id: number): Promise<Order> {
  //   // @ts-ignore
  //   return this.service.get(id, { include: [Product, Ingredient] })
  // }

  @Patch('/:id')
  async updateStatus (@Param('id') id: string, @Body('status') status): Promise<Order> {
    return this.service.update(id, { status })
  }
}
