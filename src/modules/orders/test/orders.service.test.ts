import {Test, TestingModule} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {UsersService} from "@/modules/users/users.service"
import {OrdersService} from "@/modules/orders/orders.service";
import {OrdersController} from "@/modules/orders/orders.controller";
import Order from "@/modules/orders/order.entity";
import {AuthService} from "@/modules/auth/auth.service";

const moduleMocker = new ModuleMocker(global)
const orderWithRelations = {
  userId: 1,
  cityId: 77,
  address: 'test address',
  sum: 12345,
  currency: 'р.',
  products: [
    { id: 1 },
    { id: 3 }
  ],
  ingredients: [{ id: 13 }]
}

const testOrder = {
  userId: 1,
  cityId: 77,
  address: 'test address',
  sum: 12345,
  currency: 'р.'
} as Order

const testOrderProducts = {
  productId: 1,
  orderId: 1
}
const testOrderIngredients = {
  ingredientId: 1,
  orderId: 1
}

describe('OrdersService', () => {
  let ordersService: OrdersService
  // let ordersProducts: OrdersProducts
  // let ordersIngredients: OrdersIngredients

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [OrdersController],
      providers: [OrdersService],
    //   imports: [ordersProviders]
    }).useMocker((token) => {
      switch (token) {
        case 'OrdersRepository': return {}
        case AuthService: return {}
        case UsersService: return {}
        // case OrdersProducts:
        //   return {
        //     create: jest.fn().mockRejectedValue(Promise.resolve(testOrderProducts))
        //   }
        // case OrdersIngredients:
        //   return {
        //     create: jest.fn().mockRejectedValue(Promise.resolve(testOrderIngredients))
        //   }
        case 'function':
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        default:
          break
      }
    })
      .compile()

    ordersService = moduleRef.get<OrdersService>(OrdersService)
    // ordersProducts = moduleRef.get<OrdersProducts>(OrdersProducts)
    // ordersIngredients = moduleRef.get<OrdersIngredients>(OrdersIngredients)
    // jwtService = moduleRef.get<JwtService>(JwtService)
  })

  describe('createWithRelations', () => {
    it('Should ...', async () => {
      jest.spyOn(ordersService, 'create').mockImplementation(() => Promise.resolve(testOrder))
      const { products, ingredients, ...orderData } = orderWithRelations

      // TODO::FIX
      // @ts-ignore
      const result = await ordersService.createWithRelations(orderWithRelations)
      expect(ordersService.create).toHaveBeenCalled()
      expect(ordersService.create).toHaveBeenCalledWith(orderData)
      // expect(OrdersProducts.create).toHaveBeenCalled()
      // expect(OrdersProducts.create).toHaveBeenCalledWith(orderData)
      // expect(OrdersIngredients.create).toHaveBeenCalled()
      // expect(OrdersIngredients.create).toHaveBeenCalledWith(orderData)
      expect(result).toEqual(expect.objectContaining(orderData as Order))
    })
  })
})

