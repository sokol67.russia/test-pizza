import {Test} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {OrdersService} from "@/modules/orders/orders.service";
import {OrdersController} from "@/modules/orders/orders.controller";
import Order from "@/modules/orders/order.entity";

const moduleMocker = new ModuleMocker(global)

describe('OrdersController', () => {
  let ordersController: OrdersController
  let ordersService: OrdersService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [OrdersController],
      // providers: [OrdersService],
    }).useMocker((token) => {
        if (token === OrdersService) {
          return {
            createWithRelations: jest.fn().mockResolvedValue(Promise.resolve({} as Order)),
            update: jest.fn().mockResolvedValue(Promise.resolve({} as Order))
          }
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        }
      }).compile()

    ordersService = moduleRef.get<OrdersService>(OrdersService)
    ordersController = moduleRef.get<OrdersController>(OrdersController)
  })

  describe('create', () => {
    it('should call the create service method', async () => {
      const newOrder = {
        userId: 1,
        cityId: 77,
        address: "test address",
        sum: 12345,
        currency: "р.",
        products: [
          { "id": 1 },
          { "id": 3 }
        ],
        ingredients: [{ "id": 13 }]
      }

      const result = await ordersController.create(newOrder)
      expect(ordersService.createWithRelations).toHaveBeenCalled()
      expect(ordersService.createWithRelations).toHaveBeenCalledWith(newOrder, undefined)
      expect(result).toEqual(expect.objectContaining({}))
    })
  })

  describe('updateStatus', () => {
    it('should call the updateStatus service method', async () => {
      const result = await ordersController.updateStatus('1', 'inDelivery')
      expect(ordersService.update).toHaveBeenCalled()
      expect(ordersService.update).toHaveBeenCalledWith('1', { status: 'inDelivery' })
      expect(result).toEqual(expect.objectContaining({}))
    })
  })
})
