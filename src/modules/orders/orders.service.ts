import {Inject, Injectable} from '@nestjs/common'
import {SequelizeService} from "@/lib/sequelize.service";
import Order from "@/modules/orders/order.entity";
import {ordersRepositoryProvider} from "@/modules/orders/orders.providers";
import {CreateOptions} from "sequelize";
import OrdersProducts from "@/modules/database/orders-products.entity";
import OrdersIngredients from "@/modules/database/orders-ingridients.entity";

@Injectable()
export class OrdersService extends SequelizeService<Order> {
  constructor(
    @Inject(ordersRepositoryProvider.provide)
    readonly repository: typeof Order,
  ) {
    super(repository)
  }

  async createWithRelations ({ products, ingredients, ...orderData }: {
    products: { id: number }[];
    ingredients: any;
    orderData: Order
  }, options?: CreateOptions): Promise<Order> {
    const order = await this.create(orderData, options)

    await Promise.all(products.map(product => OrdersProducts.create({
      productId: product.id,
      orderId: order.id
    } as OrdersProducts)))

    await Promise.all(ingredients.map(ingredient => OrdersIngredients.create({
      ingredientId: ingredient.id,
      orderId: order.id
    } as OrdersIngredients)))

    return order
  }
}
