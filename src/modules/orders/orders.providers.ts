import Order from '@/modules/orders/order.entity'
import { Provider, ValueProvider } from '@nestjs/common/interfaces'

export const ordersRepositoryProvider: ValueProvider = {
  provide: 'OrdersRepository',
  useValue: Order
}

export const ordersProviders: Provider[] = [ordersRepositoryProvider]
