import { Module } from '@nestjs/common'
import { OrdersService } from "@/modules/orders/orders.service";
import {OrdersController} from "@/modules/orders/orders.controller";
import {ordersProviders} from "@/modules/orders/orders.providers";

@Module({
  imports: [],
  controllers: [OrdersController],
  providers: [OrdersService, ...ordersProviders],
  exports: [OrdersService, ...ordersProviders]
})
export class OrdersModule {}
