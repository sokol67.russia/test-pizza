import {
  AllowNull,
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  CreatedAt, DataType,
  Default,
  DeletedAt, ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt
} from 'sequelize-typescript'
import User from "@/modules/users/user.entity";
import {HasManyGetAssociationsMixin, HasOneGetAssociationMixin, Association} from "sequelize";
import Product from "@/modules/products/product.entity";
import Ingredient from "@/modules/Ingredients/ingridient.entity";
import OrdersIngredients from "@/modules/database/orders-ingridients.entity";
import City from "@/modules/cities/city.entity";
import OrdersProducts from "@/modules/database/orders-products.entity";

@Table({
  modelName: 'orders'
})
export class Order extends Model<Order> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column
  userId: number

  @ForeignKey(() => City)
  @Column
  cityId: number

  @Column
  accepted: Date

  @Default('inProcess')
  @Column(DataType.ENUM(...[
    'inProcess',
    'inDelivery',
    'rejected'
  ]))
  status: 'inProcess' | 'inDelivery' | 'rejected'

  @Column
  address: string;

  @Default(0)
  @AllowNull(false)
  @Column
  sum: number

  @Column
  currency: string;

  @Column
  completedAt: Date

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  @DeletedAt
  deletedAt: Date

  @BelongsTo(() => User)
  user: User

  @BelongsTo(() => City)
  city: City

  @BelongsToMany(() => Product, () => OrdersProducts)
  products: Product[]

  @BelongsToMany(() => Ingredient, () => OrdersIngredients)
  ingredients: Ingredient[]

  getUser!: HasOneGetAssociationMixin<User>
  getCity!: HasOneGetAssociationMixin<City>
  getProducts!: HasManyGetAssociationsMixin<Product>
  getIngredients!: HasManyGetAssociationsMixin<Ingredient>

  static associations: {
    user: Association<Order, User>
    city: Association<Order, City>
    products: Association<Order, Product>
    ingredients: Association<Order, Ingredient>
  }
}

export default Order
