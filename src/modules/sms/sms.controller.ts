import {
  Controller,
  UseGuards,
  UseInterceptors,
  ClassSerializerInterceptor, Post, Req, BadRequestException, Body,
} from '@nestjs/common'
import SmsService from "@/modules/sms/sms.service";
import { Request } from 'express'
import { AuthGuard } from "@/modules/auth/auth.guard"
import User from "@/modules/users/user.entity";

@Controller('sms')
@UseInterceptors(ClassSerializerInterceptor)
export default class SmsController {
  constructor(
    private readonly smsService: SmsService
  ) {}

  // @Post('initiate-verification')
  // @UseGuards(AuthGuard)
  // async initiatePhoneNumberVerification(@Req() req: Request) {
  //   const user = req.user  as User
  //   if (user.isPhoneConfirmed) throw new BadRequestException('Номер телефона уже подтверждён')
  //   await this.smsService.initiatePhoneVerification(user.phone)
  // }

  @Post('check-verification-code')
  @UseGuards(AuthGuard)
  async checkVerificationCode(@Req() req: Request, @Body() verificationData) {
    const user = req.user  as User
    if (user.isPhoneConfirmed) throw new BadRequestException('Номер телефона уже подтверждён')
    await this.smsService.confirmPhoneNumber(user.id, user.phone, verificationData.code)
  }
}
