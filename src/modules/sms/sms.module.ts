import { Global, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import * as Joi from '@hapi/joi'
import SmsService from '@/modules/sms/sms.service'

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        TWILIO_ACCOUNT_SID: Joi.string().required(),
        TWILIO_AUTH_TOKEN: Joi.string().required(),
        TWILIO_VERIFICATION_SERVICE_SID: Joi.string().required(),
        TWILIO_SENDER_PHONE_NUMBER: Joi.string().required(),
        // ...
      })
    }),
  ],
  providers: [SmsService],
  exports: [SmsService]
})
export class SmsModule {}
