import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import City from "@/modules/cities/city.entity";
import {citiesRepositoryProvider} from "@/modules/cities/cities.providers";

@Injectable()
export class CitiesService extends SequelizeService<City> {
  constructor(
    @Inject(citiesRepositoryProvider.provide)
    readonly repository: typeof City,
  ) {
    super(repository)
  }
}
