import { Controller } from '@nestjs/common'
import { SequelizeCrudController } from '@/lib/sequelize-crud.controller'
import City from "@/modules/cities/city.entity";
import {CitiesService} from "@/modules/cities/cities.service";

@Controller('cities')
export class CitiesController extends SequelizeCrudController<CitiesService, City> {
  constructor(readonly service: CitiesService) {
    super(service)
  }
}
