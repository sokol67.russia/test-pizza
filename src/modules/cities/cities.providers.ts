import { Provider, ValueProvider } from '@nestjs/common/interfaces'
import City from "@/modules/cities/city.entity";

export const citiesRepositoryProvider: ValueProvider = {
  provide: 'CitiesRepository',
  useValue: City
}

export const citiesProviders: Provider[] = [citiesRepositoryProvider]
