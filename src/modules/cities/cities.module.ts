import {Module} from '@nestjs/common'
import {citiesProviders} from "@/modules/cities/cities.providers";
import {CitiesService} from "@/modules/cities/cities.service"
import {CitiesController} from "@/modules/cities/cities.controller"

@Module({
  imports: [],
  controllers: [CitiesController],
  providers: [CitiesService, ...citiesProviders],
  exports: [CitiesService, ...citiesProviders]
})
export class CitiesModule {}
