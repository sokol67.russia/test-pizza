import {AllowNull, AutoIncrement, Column, HasMany, Model, PrimaryKey, Table} from 'sequelize-typescript'
import User from "@/modules/users/user.entity";
import {HasManyGetAssociationsMixin, Association} from "sequelize";

@Table({
  modelName: 'cities',
  timestamps: false
})
export class City extends Model<City> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  name!: string

  @AllowNull(false)
  @Column
  type: string

  @AllowNull(false)
  @Column
  name_with_type: string

  @AllowNull(false)
  @Column
  federal_district: string

  @AllowNull(false)
  @Column
  kladr_id: string

  @AllowNull(false)
  @Column
  fias_id: string

  @AllowNull(false)
  @Column
  okato: string

  @AllowNull(false)
  @Column
  oktmo: string

  @AllowNull(false)
  @Column
  tax_office: string

  @AllowNull(false)
  @Column
  postal_code: string

  @AllowNull(false)
  @Column
  iso_code: string

  @AllowNull(false)
  @Column
  timezone: string

  @AllowNull(false)
  @Column
  geoname_code: string

  @AllowNull(false)
  @Column
  geoname_id: string

  @AllowNull(false)
  @Column
  geoname_name: string

  static associations: {
  }
}

export default City
