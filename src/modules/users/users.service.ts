import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import {FindOptions} from 'sequelize'
import User from "@/modules/users/user.entity";
import {usersRepositoryProvider} from "@/modules/users/users.providers";

@Injectable()
export class UsersService extends SequelizeService<User> implements OnModuleInit {
  constructor(
    @Inject(usersRepositoryProvider.provide)
    readonly repository: typeof User,
  ) {
    super(repository)
  }

  async onModuleInit() {
    console.log('users service loaded')
    return
    var aclass = "country";
    var method = "getcountries";
    var user = "user";
    var password = "password";

    const https = require('https')
    const options = {
      hostname: 'afilnet.com',
      port: 443,
      path: '/api/http/?class='+aclass+'&method='+method+'&user='+user+'&password='+password,
      method: 'GET'
    }

    console.log(1)
    const req = https.request(options, res => {
      res.on('data', d => {
        console.log(111)
        // Do something
      })
    })
    console.log(2)

    req.on('error', error => {
      console.error(error)
    })
    req.end()
    console.log(3)

    return
    // var url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fias";
    // var token = "b1de7849532e36fc7394433a063da00762d8313d";
    // var query = "москва хабар";
    //
    // var options = {
    //   method: "POST",
    //   mode: "cors",
    //   headers: {
    //     "Content-Type": "application/json",
    //     "Accept": "application/json",
    //     "Authorization": "Token " + token
    //   },
    //   body: JSON.stringify({query: query})
    // }
    // fetch(url, options)
    //   .then(response => response.text())
    //   .then(result => console.log(result))
    //   .catch(error => console.log("error", error));


    const Dadata = require('dadata-suggestions');
    const dadata = new Dadata('b1de7849532e36fc7394433a063da00762d8313d');
    dadata.address({ query: 'Невский', count: 5 })
      .then((data) => {
        console.log(data);
      })
      .catch(console.error);
    dadata.city()
  }

  async findByPhone(phone: string, options?: Exclude<FindOptions, 'where'>) {
    return this.repository.findOne({ where: { phone }, ...options })
  }

  async confirmPhone(id) {
    return this.update(id, { isPhoneConfirmed: true })
  }
}
