import {Test, TestingModule} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {UsersService} from "@/modules/users/users.service"
import {AuthService} from "@/modules/auth/auth.service";
import {UsersController} from "@/modules/users/users.controller";
import User from "@/modules/users/user.entity";

const moduleMocker = new ModuleMocker(global)
const testUser = { id: 1, name: 'test', roleId: 1, password: '72b9817c2c4af1b5157648ec11c8eb1307b1ca1c' } as User
const phone = '+79605897860'

describe('UsersService', () => {
  let usersService: UsersService

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).useMocker((token) => {
      switch (token) {
        case 'UsersRepository': return {
          findOne: jest.fn().mockImplementation(({ where }) => Promise.resolve(where.phone === phone ? testUser : undefined))
        }
        case AuthService: return {}
        case 'function':
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        default:
          break
      }
    }).compile()

    usersService = moduleRef.get<UsersService>(UsersService)
  })

  describe('findByPhone', () => {
    it('Should find user by his phone', async () => {
      const options = {}
      const result = await usersService.findByPhone(phone, options)
      expect(usersService.repository.findOne).toHaveBeenCalled()
      expect(usersService.repository.findOne).toHaveBeenCalledWith({ where: { phone }, ...options })
      expect(result).toEqual(expect.objectContaining(testUser))
    })
  })

  describe('confirmPhone', () => {
    it('Should set isPhoneConfirmed in true', async () => {
      jest.spyOn(usersService, 'update').mockImplementation(() => Promise.resolve(testUser as User))
      const id = 1

      // TODO::FIX
      // @ts-ignore
      const result = await usersService.confirmPhone(id)
      expect(usersService.update).toHaveBeenCalled()
      expect(usersService.update).toHaveBeenCalledWith(id, { isPhoneConfirmed: true })
      expect(result).toEqual(testUser)
    })
  })
})

