import {Test} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {UsersController} from "@/modules/users/users.controller";
import {UsersService} from "@/modules/users/users.service";
import User from "@/modules/users/user.entity";

const moduleMocker = new ModuleMocker(global)
const req = { user: { id: 1 } }

describe('UsersController', () => {
  let usersController: UsersController
  let usersService: UsersService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UsersController],
      // providers: [UsersService],
    }).useMocker((token) => {
        if (token === UsersService) {
          return {
            get: jest.fn().mockResolvedValue(Promise.resolve({} as User)),
            update: jest.fn().mockResolvedValue(Promise.resolve({} as User))
          }
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        }
      }).compile()

    usersService = moduleRef.get<UsersService>(UsersService)
    usersController = moduleRef.get<UsersController>(UsersController)
  })

  describe('me', () => {
    it('should call the get service method', async () => {
      const result = await usersController.me(req)
      expect(usersService.get).toHaveBeenCalled()
      expect(usersService.get).toHaveBeenCalledWith(1)
      expect(result).toEqual(expect.objectContaining({}))
    })
  })

  describe('save', () => {
    it('should call the updateStatus update method', async () => {
      const userData = { name: "Test" } as User
      const result = await usersController.save(req, userData)
      expect(usersService.update).toHaveBeenCalled()
      expect(usersService.update).toHaveBeenCalledWith(1, userData)
      expect(result).toEqual(expect.objectContaining({}))
    })
  })
})
