import {
  AllowNull,
  AutoIncrement, BelongsTo,
  Column,
  CreatedAt,
  Default,
  DeletedAt, ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt
} from 'sequelize-typescript'
import {Association} from "sequelize";
import Role from "@/modules/roles/role.entity";
import {AuthService} from "@/modules/auth/auth.service";

@Table({
  modelName: 'user'
})
export class User extends Model<User> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  name!: string

  @ForeignKey(() => Role)
  @AllowNull(false)
  @Column
  roleId!: number

  @AllowNull(false)
  @Column
  phone: string

  @Column
  email: string

  @Column
  dodoCode: number

  @Column
  birthday: Date

  @Default(false)
  @Column
  public isPhoneConfirmed: boolean;

  @Column
  get password(): string {
    return this.getDataValue('password')
    // return
  }

  set password(password: string) {
    AuthService.validatePassword(password)
    this.setDataValue('password', AuthService.createPasswordHash(password))
  }

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  @DeletedAt
  deletedAt: Date

  @BelongsTo(() => Role)
  role: Role

  static associations: {
    role: Association<User, Role>
  }
}

export default User
