import {Body, Controller, Get, Post, Req, UseGuards} from '@nestjs/common'
import {SequelizeCrudController} from '@/lib/sequelize-crud.controller'
import {AuthGuard} from "@/modules/auth/auth.guard";
import {UsersService} from "@/modules/users/users.service";
import User from "@/modules/users/user.entity";

@Controller('users')
export class UsersController extends SequelizeCrudController<UsersService, User> {
  constructor(readonly service: UsersService) {
    super(service)
  }

  @UseGuards(AuthGuard)
  @Get('/me')
  me(@Req() req: any) {
    const { id: userId }: { id: number } = req.user
    return this.service.get(userId)
  }

  @UseGuards(AuthGuard)
  @Post('/me')
  save(@Req() req: any, @Body() userData: User) {
    const { id: userId }: { id: number } = req.user
    return this.service.update(userId, userData)
  }
}
