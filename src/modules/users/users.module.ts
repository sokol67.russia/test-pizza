import {Module} from '@nestjs/common'
import {UsersController} from "@/modules/users/users.controller";
import {UsersService} from "@/modules/users/users.service";
import {usersProviders} from "@/modules/users/users.providers";

@Module({
  imports: [],
  controllers: [UsersController],
  providers: [UsersService, ...usersProviders],
  exports: [UsersService, ...usersProviders]
})
export class UsersModule {}
