import {Module} from '@nestjs/common'
import {pickupPointsProviders} from "@/modules/pickup-points/pickup-points.providers";
import {PickupPointsController} from "@/modules/pickup-points/pickup-points.controller";
import {PickupPointsService} from "@/modules/pickup-points/pickup-points.service";

@Module({
  imports: [],
  controllers: [PickupPointsController],
  providers: [PickupPointsService, ...pickupPointsProviders],
  exports: [PickupPointsService, ...pickupPointsProviders]
})
export class PickupPointsModule {}
