import { Provider, ValueProvider } from '@nestjs/common/interfaces'
import PickupPoint from "@/modules/pickup-points/pickup-point.entity";

export const pickupPointsRepositoryProvider: ValueProvider = {
  provide: 'PickupPointsRepository',
  useValue: PickupPoint
}

export const pickupPointsProviders: Provider[] = [pickupPointsRepositoryProvider]
