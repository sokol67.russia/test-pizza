import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  ForeignKey,
  Model,
  PrimaryKey,
  Table
} from 'sequelize-typescript'
import {Association} from "sequelize";
import City from "@/modules/cities/city.entity";

@Table({
  modelName: 'pickup_points',
  timestamps: false
})
export class PickupPoint extends Model<PickupPoint> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @ForeignKey(() => City)
  @AllowNull(false)
  @Column
  cityId!: number

  @AllowNull(false)
  @Column
  address!: string

  @BelongsTo(() => City)
  city: City

  static associations: {
    city: Association<PickupPoint, City>
  }
}

export default PickupPoint
