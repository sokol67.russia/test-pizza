import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import PickupPoint from "@/modules/pickup-points/pickup-point.entity";
import {pickupPointsRepositoryProvider} from "@/modules/pickup-points/pickup-points.providers";

@Injectable()
export class PickupPointsService extends SequelizeService<PickupPoint> {
  constructor(
    @Inject(pickupPointsRepositoryProvider.provide)
    readonly repository: typeof PickupPoint,
  ) {
    super(repository)
  }

  async getByCity (cityId) {
    return this.find({ where: { cityId } })
  }
}
