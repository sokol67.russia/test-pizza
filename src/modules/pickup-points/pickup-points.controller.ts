import {Controller, Get, Query} from '@nestjs/common'
import { SequelizeCrudController } from '@/lib/sequelize-crud.controller'
import City from "@/modules/cities/city.entity";
import {CitiesService} from "@/modules/cities/cities.service";
import {PickupPointsService} from "@/modules/pickup-points/pickup-points.service";
import PickupPoint from "@/modules/pickup-points/pickup-point.entity";

@Controller('pickup-points')
export class PickupPointsController extends SequelizeCrudController<PickupPointsService, PickupPoint> {
  constructor(readonly service: PickupPointsService) {
    super(service)
  }

  @Get('/city')
  async getByCity(@Query('cityId') cityId) {
    return this.service.getByCity(cityId)
  }
}
