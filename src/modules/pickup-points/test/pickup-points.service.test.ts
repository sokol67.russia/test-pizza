import {Test, TestingModule} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {UsersService} from "@/modules/users/users.service"
import PickupPoint from "@/modules/pickup-points/pickup-point.entity";
import {PickupPointsService} from "@/modules/pickup-points/pickup-points.service";
import {AuthService} from "@/modules/auth/auth.service";
import {PickupPointsController} from "@/modules/pickup-points/pickup-points.controller";

const moduleMocker = new ModuleMocker(global)
const testPoint = {
  id: 1,
  cityId: 77,
  address: 'test address',
} as PickupPoint

describe('PickupPointsService', () => {
  let pickupPointsService: PickupPointsService

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [PickupPointsController],
      providers: [PickupPointsService],
    }).useMocker((token) => {
      switch (token) {
        case 'PickupPointsRepository': return {}
        case AuthService: return {}
        case UsersService: return {}
        case 'function':
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        default:
          break
      }
    }).compile()

    pickupPointsService = moduleRef.get<PickupPointsService>(PickupPointsService)
  })

  describe('getByCity', () => {
    it('Should find pickup points of specified city', async () => {
      jest.spyOn(pickupPointsService, 'find').mockImplementation(() => Promise.resolve([testPoint]))
      const cityId = 77

      // TODO::FIX
      // @ts-ignore
      const result = await pickupPointsService.getByCity(cityId)
      expect(pickupPointsService.find).toHaveBeenCalled()
      expect(pickupPointsService.find).toHaveBeenCalledWith({ where: { cityId } })
      expect(result).toEqual(expect.objectContaining([testPoint]))
    })
  })
})

