import {Test} from '@nestjs/testing'
import {ModuleMocker} from 'jest-mock'
import {PickupPointsController} from "../pickup-points.controller";
import {PickupPointsService} from "../pickup-points.service";
import PickupPoint from "../pickup-point.entity";

const moduleMocker = new ModuleMocker(global)

describe('PickupPointsController', () => {
  let pickupPointsController: PickupPointsController
  let pickupPointsService: PickupPointsService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [PickupPointsController],
      // providers: [PickupPointsService],
    }).useMocker((token) => {
        if (token === PickupPointsService) {
          return {
            getByCity: jest.fn().mockResolvedValue(Promise.resolve({} as PickupPoint)),
          }
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        }
      }).compile()

    pickupPointsService = moduleRef.get<PickupPointsService>(PickupPointsService)
    pickupPointsController = moduleRef.get<PickupPointsController>(PickupPointsController)
  })

  describe('create', () => {
    it('should call the getByCity service method', async () => {
      const result = await pickupPointsController.getByCity(1)
      expect(pickupPointsService.getByCity).toHaveBeenCalled()
      expect(pickupPointsService.getByCity).toHaveBeenCalledWith(1)
      expect(result).toEqual(expect.objectContaining({}))
    })
  })
})
