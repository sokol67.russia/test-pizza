import {TokenPayload} from "@/modules/auth/interfaces/token-payload.interface"

export interface DecodedToken extends TokenPayload {
  iat: Date
  exp: Date
}
