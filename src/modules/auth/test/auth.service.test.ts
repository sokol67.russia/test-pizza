import {BadRequestException, NotFoundException, UnauthorizedException} from "@nestjs/common"
import {Test} from '@nestjs/testing'
import {AuthController} from "@/modules/auth/auth.controller"
import {AuthService} from "@/modules/auth/auth.service"
import {ModuleMocker} from 'jest-mock'
import User from "@/modules/users/user.entity"
import {UsersService} from "@/modules/users/users.service"
import {JwtService} from "@nestjs/jwt"
import {EnvironmentService} from "@/modules/environment/environment.service"
import sha1 from "sha1"

const moduleMocker = new ModuleMocker(global)
const testUser = { id: 1, name: 'test', roleId: 1, password: '72b9817c2c4af1b5157648ec11c8eb1307b1ca1c' } as User
const decodedToken = {
  iat: new Date(),
  exp: new Date()
}

describe('AuthService', () => {
  let authService: AuthService
  let usersService: UsersService
  let jwtService: JwtService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [AuthService],
    }).useMocker((token) => {
      switch (token) {
        case UsersService:
          return {
            findByPhone: jest.fn().mockImplementation((phone) => phone === '+79605897860' ? testUser : undefined)
          }
        case EnvironmentService:
          return {
            get: jest.fn().mockRejectedValue(null)
          }
        case JwtService:
          return {
            sign: jest.fn().mockReturnValue('token'),
            verify: jest.fn().mockResolvedValue(decodedToken)
          }
        case 'function':
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        default:
          break
      }
    }).compile()

    authService = moduleRef.get<AuthService>(AuthService)
    usersService = moduleRef.get<UsersService>(UsersService)
    jwtService = moduleRef.get<JwtService>(JwtService)
    AuthService.secret = 'superSecret'
  })

  describe('createAuthToken', () => {
    it('Should throw exception, if the user was not found', () => expect(() => authService.createAuthToken(null))
      .toThrow(new NotFoundException('Пользователь не найден')))
    it('Should return token', () => expect(authService.createAuthToken({id: 1} as User, '30d')).toBe('token'))
  })

  describe('login', () => {
    it('Should throw exception, if the phone was not specified', () => expect(authService.login('', ''))
      .rejects.toThrow(new BadRequestException('Не указан телефон')))
    it('Should throw exception, if the user was not found', () => expect(authService.login('+7', ''))
      .rejects.toThrow(new NotFoundException(`Пользователь с таким телефоном не найден`)))
    it('Should throw exception, if the password was incorrect', () => expect(authService.login('+79605897860', 'mistake'))
      .rejects.toThrow(new UnauthorizedException('Неверный пароль')))
    it('should return token and user', async () => {
      jest.spyOn(authService, 'createAuthToken').mockImplementation(() => 'token')
      const result = await authService.login('+79605897860', 'password')
      expect(usersService.findByPhone).toHaveBeenCalled()
      expect(usersService.findByPhone).toHaveBeenCalledWith('+79605897860')
      expect(authService.createAuthToken).toHaveBeenCalled()
      expect(authService.createAuthToken).toHaveBeenCalledWith(testUser)
      expect(result).toEqual(expect.objectContaining(['token', testUser]))
    })
  })

  describe('validateAuthToken', () => {
    it('Should throw exception, if the password was incorrect', () =>expect(() => authService.validateAuthToken(''))
        .toThrow(new UnauthorizedException('Отсутствует токен аутентификации')))
    it('should validate the token properly', async () => {
      const token = 'token'
      const result = await authService.validateAuthToken(token)

      expect(jwtService.verify).toHaveBeenCalled()
      expect(jwtService.verify).toHaveBeenCalledWith(token)
      expect(result).toEqual(expect.objectContaining(decodedToken))
    })
  })

  describe('createPasswordHash', () => {
    it('Should create uniq hash', () => {
      const password = 'password'
      const hash = sha1(`pass${AuthService.secret}word`)
      expect(AuthService.createPasswordHash(password)).toBe(hash)
    })
  })

  describe('validatePassword', () => {
    it('Should throw exception, if the password not specified', () => expect(() => AuthService.validatePassword(''))
      .toThrow(new BadRequestException('Не указан пароль')))
    it('Should throw exception, if the password is too short', () => expect(() => AuthService.validatePassword('1234'))
      .toThrow(new BadRequestException('Пароль не может быть короче 5 символов')))
    it('Should create uniq hash', () => expect(AuthService.validatePassword('password')).toBe(true))
  })
})

