import { forwardRef, Global, Module } from '@nestjs/common'
import { JwtModule } from '@nestjs/jwt'
// TODO:: FIX!
import { AuthController } from './auth.controller'
import {UsersModule} from "@/modules/users/users.module";
import {AuthService} from "@/modules/auth/auth.service";
import {EnvironmentService} from "@/modules/environment/environment.service";

@Global()
@Module({
  imports: [
    JwtModule.registerAsync({
      useFactory(envService: EnvironmentService) {
        return {
          secret: envService.get('JWT_SECRET', 'superSecret'),
          signOptions: { expiresIn: '30d' }
        }
      },
      inject: [EnvironmentService]
    }),
    forwardRef(() => UsersModule)
  ],
  providers: [AuthService],
  controllers: [AuthController],
  exports: [AuthService, UsersModule]
})
export class AuthModule {}
