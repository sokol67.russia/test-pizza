import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { Request, Response } from 'express'
import {UsersService} from "@/modules/users/users.service";
import {AuthService} from "@/modules/auth/auth.service";
import * as _ from 'lodash'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly authService: AuthService, private readonly usersService: UsersService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const http = context.switchToHttp()
    const request: Request = http.getRequest()
    const response: Response = http.getResponse()
    const token = AuthGuard.getToken(request)

    const { userId } = this.authService.validateAuthToken(token)
    request.user = await this.usersService.get(userId, { rejectOnEmpty: true })
    response.cookie('authorization', token, { maxAge: 1000 * 60 * 60 * 24 * 7, sameSite: 'none', secure: true })

    return true
  }

  static getToken (request) {
    if (request.query.token) return request.query.token

    const authorization = _.get(request, 'headers.authorization', '') || _.get(request, 'cookies.authorization', '')
    const bearerMatches = authorization.match(/Bearer (.+)/)
    return bearerMatches ? bearerMatches[1] : authorization
  }
}
