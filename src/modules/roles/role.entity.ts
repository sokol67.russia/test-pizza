import {AllowNull, AutoIncrement, Column, HasMany, Model, PrimaryKey, Table} from 'sequelize-typescript'
import User from "@/modules/users/user.entity";
import {HasManyGetAssociationsMixin, Association} from "sequelize";

@Table({
  modelName: 'role',
  timestamps: false
})
export class Role extends Model<Role> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  name!: string

  @HasMany(() => User)
  users: User[]

  getUsers!: HasManyGetAssociationsMixin<User>

  static associations: {
    users: Association<Role, User>
  }
}

export default Role
