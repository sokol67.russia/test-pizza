import {Module} from '@nestjs/common'
import {rolesProviders} from "@/modules/roles/roles.providers";
import {RolesService} from "@/modules/roles/roles.service"

@Module({
  imports: [],
  // controllers: [RolesController],
  providers: [RolesService, ...rolesProviders],
  exports: [RolesService, ...rolesProviders]
})
export class RolesModule {}
