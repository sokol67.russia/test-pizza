import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  BelongsToMany,
  Column,
  ForeignKey,
  Model,
  PrimaryKey,
  Table
} from 'sequelize-typescript'
import Product from "@/modules/products/product.entity";
import Order from "@/modules/orders/order.entity";
import OrdersIngredients from "@/modules/database/orders-ingridients.entity";
import {Association} from "sequelize";
import ProductsIngredients from "@/modules/database/products-ingridients.entity";

@Table({
  modelName: 'ingredients',
  timestamps: false
})
export class Ingredient extends Model<Ingredient> {
  @AutoIncrement
  @PrimaryKey
  @Column
  readonly id!: number

  @AllowNull(false)
  @Column
  name!: string

  @Column
  public energyValue: string

  @BelongsToMany(() => Product, () => ProductsIngredients)
  products: Product[]

  @BelongsToMany(() => Order, () => OrdersIngredients)
  orders: Order[]

  static associations: {
    products: Association<Ingredient, Product>
    orders: Association<Ingredient, Order>
  }
}

export default Ingredient
