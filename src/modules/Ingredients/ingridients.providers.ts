import { Provider, ValueProvider } from '@nestjs/common/interfaces'
import Ingredient from '@/modules/Ingredients/ingridient.entity'

export const ingredientsRepositoryProvider: ValueProvider = {
  provide: 'IngredientsRepository',
  useValue: Ingredient
}

export const ingredientsProviders: Provider[] = [ingredientsRepositoryProvider]
