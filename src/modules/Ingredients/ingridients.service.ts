import {forwardRef, Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import Ingredient from "@/modules/Ingredients/ingridient.entity";
import {ingredientsRepositoryProvider} from "@/modules/Ingredients/ingridients.providers";
import {ProductsService} from "@/modules/products/products.service";

@Injectable()
export class IngredientsService extends SequelizeService<Ingredient> {
  constructor(
    @Inject(ingredientsRepositoryProvider.provide)
    readonly repository: typeof Ingredient,
    @Inject(forwardRef(() => ProductsService))
    protected readonly productsService: ProductsService,
  ) {
    super(repository)
  }
}
