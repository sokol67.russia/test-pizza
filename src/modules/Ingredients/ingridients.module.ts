import {forwardRef, Module} from '@nestjs/common'
import {IngredientsService} from "@/modules/Ingredients/ingridients.service";
import {ingredientsProviders} from "@/modules/Ingredients/ingridients.providers";
import {ProductsModule} from "@/modules/products/products.module"

@Module({
  imports: [forwardRef(() => ProductsModule)],
  // controllers: [IngredientsController],
  providers: [IngredientsService, ...ingredientsProviders],
  exports: [IngredientsService, ...ingredientsProviders]
})
export class IngredientsModule {}
