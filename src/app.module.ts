import { Module } from '@nestjs/common'
import {AuthModule} from "@/modules/auth/auth.module"
import {EnvironmentModule} from "@/modules/environment/environment.module"
import {SmsModule} from "@/modules/sms/sms.module"
import {CitiesModule} from '@/modules/cities/cities.module'
import {ProductsModule} from '@/modules/products/products.module'
import {IngredientsModule} from "@/modules/Ingredients/ingridients.module";
import {OrdersModule} from "@/modules/orders/orders.module";
import {PickupPointsModule} from "@/modules/pickup-points/pickup-points.module";
import {PromoCodesModule} from "@/modules/promo-codes/promo-codes.module";
import {RolesModule} from "@/modules/roles/roles.module";
import {UsersModule} from "@/modules/users/users.module";
import {VariationsModule} from "@/modules/variations/variations.module";
import {AppService} from "@/app.service";
import {AppController} from "@/app.controller";
import {DatabaseModule} from "@/modules/database/database.module";

@Module({
  imports: [
    DatabaseModule,
    AuthModule,
    EnvironmentModule,
    SmsModule,
    CitiesModule,
    ProductsModule,
    IngredientsModule,
    OrdersModule,
    PickupPointsModule,
    PromoCodesModule,
    RolesModule,
    UsersModule,
    VariationsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
