import * as _ from 'lodash'
import { Association, IncludeOptions, Op, OrderItem, WhereOptions } from 'sequelize'
import { DataType, ModelCtor } from 'sequelize-typescript'
import { operatorAliases } from '@/modules/database/operator-aliases'

type TypedObject = { [key: string]: string | TypedObject }

export class SequelizeQueryParser {
  private readonly options: Options = {
    where: { [Op.and]: [] },
    include: [],
    order: []
  }

  constructor(readonly repository: ModelCtor, readonly query: Query) {}

  getOptions() {
    this.parseFilter()
    this.parseIncludes()
    this.parseSort()
    return this.options
  }

  parseFilter() {
    const { filter } = this.query
    if (!filter || !_.isObject(filter)) return
    this.parseLoop(filter)
  }

  parseLoop(
    filter: any,
    target = this.repository,
    options = this.options,
    associationsChain: Association[] = []
  ): void {
    options.where[Op.and] = options.where[Op.and] || []

    _.forEach(filter, (value, name) => {
      const replaced = this.transformValue(value)
      const attributes = target.rawAttributes
      const attribute = attributes[name]

      if (attribute && !(attribute.type instanceof DataType.VIRTUAL)) {
        return (options.where[name] = replaced)
      }

      const association = target.associations[name]

      if (association) {
        let includeOptions = options.include.find(i => i.association === association)
        if (!includeOptions) {
          includeOptions = { association, where: {}, include: [], attributes: [] }
          options.include.push(includeOptions)
        }

        this.parseLoop(
          value,
          association.target as ModelCtor,
          {
            include: includeOptions.include as IncludeOptions[],
            where: includeOptions.where
          },
          [...associationsChain, association]
        )
      }
    })
  }

  parseIncludes(): void {
    const { with: includes } = this.query
    this.toArray<string>(includes).forEach(path => this.parseIncludeLoop(path.split('.')))
  }

  parseIncludeLoop(path: string[], target = this.repository, options = this.options): void {
    const [associationName = '', ...restPath] = path
    const association = target.associations[associationName.replace(/\?/, '')]
    if (!association) return

    let includeOptions: IncludeOptions = options.include.find(i => i.association === association)
    if (!includeOptions) {
      includeOptions = { association, include: [] }
      if (/\?/.test(associationName)) includeOptions.required = false
      options.include.push(includeOptions)
    } else {
      delete includeOptions.attributes
    }

    this.parseIncludeLoop(restPath, association.target as ModelCtor, {
      include: includeOptions.include as IncludeOptions[]
    })
  }

  parseSort() {
    const { sort } = this.query
    this.toArray<string>(sort).forEach(i => this.parseSortLoop(i))
  }

  parseSortLoop(sort: string) {
    const matches = sort.match(SequelizeQueryParser.sortRegEx)
    if (!matches) return

    const { attribute, directionAlias = '+' } = matches.groups
    const direction = directionAlias === '+' ? 'ASC' : 'DESC'

    this.options.order.push([attribute, direction])
  }

  protected toArray<T = any>(item: any): T[] {
    if (!item) return []
    return Array.isArray(item) ? item : [item]
  }

  protected transformValue(object: TypedObject | string) {
    const isObject = _.isObject(object)
    const isArray = Array.isArray(object)

    if (isObject && !isArray) _.forEach(object, (value, key) => {
      const op = SequelizeQueryParser.operatorAliases[key]
      if (op) delete object[key]
      if (op === Op.iLike) value = `%${value}%`

      object[op || key] = this.transformValue(value)
    })

    return object
  }

  static sortRegEx = /^(?:(?<directionAlias>-)|)(?<attribute>.+?)$/
  static operatorAliases = operatorAliases
}

export interface Options {
  where?: WhereOptions
  include?: IncludeOptions[]
  order?: OrderItem[]
}

export interface Query {
  filter?: FiltersQuery
  with?: string | string[]
  sort?: string | string[]
}

export interface FiltersQuery {
  [key: string]: string | string[] | FiltersQuery
}
