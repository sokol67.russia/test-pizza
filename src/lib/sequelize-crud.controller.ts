import {
  Body,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {ExtendedUpdateOptions, SequelizeService} from "@/lib/sequelize.service";
import { Model } from 'sequelize-typescript'
import { Options, SequelizeQueryParser} from "@/lib/sequelize-query-parser";
import { AuthGuard } from '@/modules/auth/auth.guard'
import {CreateOptions, InstanceDestroyOptions} from "sequelize";

export abstract class SequelizeCrudController<S extends SequelizeService<M>, M extends Model<M>> {
  protected constructor(readonly service: S, readonly QueryParser = SequelizeQueryParser) {}

  @UseGuards(AuthGuard)
  @Get(':id')
  async get(@Param('id') id: string, @Request() req: any, @Query() query: any) {
    // @ts-ignore
    return this.service.get(id, { ...this.parseQuery(query), rejectOnEmpty: true })
  }

  @UseGuards(AuthGuard)
  @Get()
  async findAll(@Request() req: any, @Query() query: any) {
    // @ts-ignore
    return await this.service.find({ ...this.parseQuery(query), scopes: [...(query.scopes || [])] })
  }

  @UseGuards(AuthGuard)
  @Post()
  async create(@Request() req: any, @Body() data: object, options?: CreateOptions) {
    return await this.service.create(data, options)
  }

  @UseGuards(AuthGuard)
  @Patch(':id')
  async update(@Request() { user }: any, @Param('id') id: string, @Body() data: object, options?: ExtendedUpdateOptions) {
    if (id === 'me') id = user.id
    const model = await this.service.get(id, { rejectOnEmpty: true })
    return this.service.update(model, data, options)
  }

  @UseGuards(AuthGuard)
  @Delete(':id')
  async remove(@Request() req: any, @Param('id') id: string, options?:InstanceDestroyOptions) {
    return await this.service.remove(Number(id), options)
  }

  parseQuery(query: any): Partial<Options> {
    const parser = new this.QueryParser(this.service.repository, query)
    return parser.getOptions()
  }
}
