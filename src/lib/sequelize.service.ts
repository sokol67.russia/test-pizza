import {
  CountOptions,
  CreateOptions,
  FindOptions,
  FindOrCreateOptions,
  Identifier,
  InstanceDestroyOptions,
  InstanceUpdateOptions,
  Op,
  Transaction,
  TransactionOptions,
  UpdateOptions,
  UpsertOptions
} from 'sequelize'
import { Model, ModelCtor } from 'sequelize-typescript'
import { NonNullFindOptions } from 'sequelize/types/lib/model'
import { NotFoundException } from '@nestjs/common'

export type ForceGet = {
  forceGet?: boolean
}

export type ExtendedUpdateOptions = InstanceUpdateOptions & ForceGet

export abstract class SequelizeService<M extends Model<M>> {
  protected constructor(readonly repository: ModelCtor<M>) {}

  async get(i: Identifier | M, options: Partial<NonNullFindOptions> & ForceGet = {}): Promise<M> {
    const instance = i instanceof Model ? i : null
    const identifier = instance ? (i as M).id : i

    if (instance && !options.forceGet) return instance
    else {
      try {
        // @ts-ignore
        return await this.repository.findByPk<M>(identifier, options)
      } catch (e) {
        if (!options.rejectOnEmpty) throw e
        else if (e.name === 'SequelizeEmptyResultError') this.notFound(identifier)
      }
    }
  }

  async find(options: FindOptions = {}): Promise<M[]> {
    return this.repository.findAll<M>(options)
  }

  async findByIds(ids: Identifier[], options: FindOptions = {}): Promise<M[]> {
    options.where = options.where || {}
    options.where[Op.and] = options.where[Op.and] || []
    options.where[Op.and].push({ id: { [Op.in]: ids } })
    return this.repository.findAll<M>(options)
  }

  async create(values?: object, options?: CreateOptions): Promise<M> {
    // @ts-ignore
    return this.repository.create<M>(values, options)
  }

  async update(i: Identifier | M, keys: object, options: ExtendedUpdateOptions = {}): Promise<M> {
    const instance = await this.get(i, { rejectOnEmpty: true, forceGet: options.forceGet })
    // @ts-ignore
    return instance.update(keys, options)
  }

  async bulkUpdate(values: object, options: UpdateOptions = { where: {} }) {
    // @ts-ignore
    return this.repository.update(values, { ...options, returning: true })
  }

  async upsert(values: object, options?: UpsertOptions) {
    // @ts-ignore
    return this.repository.upsert<M>(values, { ...options, returning: true })
  }

  async remove(i: Identifier | M, options?: InstanceDestroyOptions): Promise<void> {
    const instance = await this.get(i)
    return instance.destroy(options)
  }

  async bulkRemove(options): Promise<number> {
    options.returning = true
    return this.repository.destroy(options)
  }

  async findOrCreate(options: FindOrCreateOptions): Promise<[M, boolean]> {
    return this.repository.findOrCreate<M>(options)
  }

  count(options: CountOptions) {
    return this.repository.count(options)
  }

  private notFound(id: Identifier) {
    throw new NotFoundException(`${this.repository.options.name.singular} с ID ${id} не найден`)
  }

  public async createTransaction(options?: TransactionOptions): Promise<Transaction> {
    return this.repository.sequelize.transaction(options)
  }
}
