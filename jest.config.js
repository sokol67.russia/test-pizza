/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

module.exports = {
  verbose: true,
  testEnvironment: 'node',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  // "transform": {
  //   ".(ts|tsx)": "<rootDir>/node_modules/ts-jest/preprocessor.js"
  // },
  testRegex: "(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$",
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js"
  ],
  preset: 'ts-jest',
  setupFiles: ['dotenv/config'],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1"
  }
}
